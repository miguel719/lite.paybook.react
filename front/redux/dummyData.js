const dummyData = {}

	dummyData.credentials = [
		{
			id_site: "56de2130784806d7028b4589",
			avatar:"/images/52e80c49dacf4353318b4568/avatar",
		},
		{
			id_site: "56cf5728784806f72b8b456b",
			avatar: "/images/5291648bdacf43ee168b4567/avatar",

		},

	]

	dummyData.catalogs = [
			{
				id_site:"ewqewq",
				name: "Bancomer",
				credentials:[
					{
			        name: "credential1",
			        type: "text",
			        label: "Credential",
			        required: true,
			        username: true,
			        validation: null
			       },
			      {
			       name: "credential2",
			       type: "password",
			       label: "pass",
			       required: true,
			       username: false,
			       validation: null
			      }
				]
			},
			{
				id_site:"ewqewq",
				name: "Banamex",
				credentials:[
					{
			        name: "credential1",
			        type: "text",
			        label: "Credential",
			        required: true,
			        username: true,
			        validation: null
			       },
			      {
			       name: "credential2",
			       type: "password",
			       label: "pass",
			       required: true,
			       username: false,
			       validation: null
			      }
				]
			},
			{
				id_site:"ewqewq",
				name: "HSBC",
				credentials:[
					{
			        name: "credential1",
			        type: "text",
			        label: "Credential",
			        required: true,
			        username: true,
			        validation: null
			       },
			      {
			       name: "credential2",
			       type: "password",
			       label: "pass",
			       required: true,
			       username: false,
			       validation: null
			      }
				]
			},
			{
				id_site:"ewqewq",
				name: "Santander",
				credentials:[
					{
			        name: "credential1",
			        type: "text",
			        label: "Credential",
			        required: true,
			        username: true,
			        validation: null
			       },
			      {
			       name: "credential2",
			       type: "password",
			       label: "pass",
			       required: true,
			       username: false,
			       validation: null
			      }
				]
			}
	]


export default dummyData;