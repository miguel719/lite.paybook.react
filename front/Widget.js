/**
 * In this file, we create a React component
 * which incorporates components providedby material-ui.
 */

import React from 'react';
import { connect  } from 'react-redux'
import store from './redux/store'


var Widget = React.createClass({
	render: function() {
		return (
			<div>
				<h2> Widget </h2>	

				<div class="row">
		            <div class="col-sm-12">
		              <div class="panel panel-info">
		               
		                <div class="panel-body">
		                  <script type="text/javascript" src="https://www.paybook.com/lib/js/widget/widget.js"></script>
		                  <script type="text/javascript">
		                    {/* Uncoment to use development environment
		                    // pbWidget.setDev();
		                    var data_js = {{data|tojson}}
		                    var data_json = JSON.parse(data_js)
		                    var token = data_json['token']                    
		                    pbWidget.setToken(token);
		                    pbWidget.chooseBank(); */  }                 
		                  </script>
		                  <div id="paybook-container"></div>
		                </div>
		              </div>
		            </div>
		        </div>

		    </div>			
			
		);
	}
});


export default Widget;
